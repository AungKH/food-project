@extends('welcome')

@section('content')


<div class="container">
  <form method="post" action="{{ route('menus.store') }}" 
		enctype="multipart/form-data">
    @csrf
    <div class="image">
      <label><h4>Add image</h4></label>
      <input type="file" class="form-control"  name="image">
    </div>

    <div class="name">
        <label><h4>Add Name</h4></label>
        <input type="text" class="form-control"  name="name">
    </div><br>

    <div class="post_button">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </form>
</div>