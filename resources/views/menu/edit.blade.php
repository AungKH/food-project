@extends('welcome')

@section('content')


<div class="container">
  <form method="post" action="{{route('menu.update',$menu->id)}}" 
		enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="image">
      <label><h4>Old image</h4></label>
      <br>
      <input type="hidden" name="old_image" value="{{$menu->image}}" />
      <img src="{{asset($menu->image)}}" width="100px">
    </div>
 
    <div class="new_image">
      <label><h4>New image</h4></label>
      <input type="file" class="form-control"  name="new_image"><br>
    </div>

    <div class="name">
        <label><h4>Add Name</h4></label>
        <input type="text" class="form-control" value="{{$menu->name}}" name="name">
    </div><br>

    <div class="post_button">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </form>
</div>