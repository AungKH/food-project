@extends('welcome')

@section('content')

@section('script')
<div class="container">
    <h3>View all image</h3><hr>
    <table class="table">
      <thead>

        <tr>
          <button>
          <a class="btn btn-success" href="{{route('menus.add')}}" style="text-decoration:none;padding:10px;">
            Create Menu +
          </a>
          </button>
          
        </tr>
        <tr>
          <th scope="col">Image id</th>
          <th scope="col">Image</th>
          <th scope="col">Name</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($imageData as $data)
        <tr>
          <td>{{$data->id}}</td>
          <td>
            <img src="{{ asset($data->image) }}"
            style="height: 100px; width: 150px;">
	        </td> 
            <td>
                {{$data->name}}
            </td>
            <td>
                <a href="{{route('menu.edit',$data->id)}}" class="btn btn-primary">Edit</a>

                <form action="{{route('menu.destroy',$data->id)}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" onclick="return confirm('Are you sure to delete?')">
                    Delete
                  </button>
                </form>

            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

  


  
