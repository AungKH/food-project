@extends('welcome')

@section('content')


<div class="container">
  <form method="post" action="{{ route('shops.store') }}" 
		enctype="multipart/form-data">
    @csrf
   
    <div class="name">
        <label><h4>Add Name</h4></label>
        <input type="text" class="form-control" required name="name">
    </div><br>

    <div class="logo">
      <label><h4>Logo</h4></label>
      <input type="file" class="form-control" required name="logo">
    </div>

    <div class="address">
      <label><h4>Address</h4></label>
      <input type="text" class="form-control" required name="address">
    </div>

    <div class="post_button">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </form>
</div>