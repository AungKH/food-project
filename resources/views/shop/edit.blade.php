@extends('welcome')
@section('content')

<div class="container">
  <form method="post" action="{{ route('shop.update',$shop->id) }}" 
		enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="name">
        <label><h4>Add Name</h4></label>
        <input type="text" class="form-control" value="{{$shop->name}}" name="name">
    </div><br>

    <div class="logo">
      <label><h4>Old Logo</h4></label>
      <input type="hidden" name="old_logo" value="{{$shop->logo}}">
      <img src="{{asset($shop->logo)}}" width="100px">
    </div>

    <div class="logo">
      <label><h4>New Logo</h4></label>
      <input type="file" class="form-control" name="new_logo">
    </div>

    <div class="address">
      <label><h4>Address</h4></label>
      <input type="text" class="form-control" value="{{$shop->address}}" name="address">
    </div>

    <div class="post_button">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </form>
</div>