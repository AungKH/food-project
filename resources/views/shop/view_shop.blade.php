@extends('welcome')

@section('content')

<div class="container">
    <h3>View all image</h3><hr>

    <a class="btn btn-success" href="{{route('shops.add')}}" style="text-decoration:none;padding:10px;">
            Create Shop +
          </a>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Image id</th>
          <th scope="col">Name</th>
          <th scope="col">Address</th>
          <th scope="col">Logo</th>
          <th scope="col">Action</th>

        </tr>
      </thead>
      <tbody>
        @foreach($imageData as $data)
        <tr>
          <td>{{$data->id}}</td>

            <td>
                {{$data->name}}
            </td>  

            <td>
                {{$data->address}}
            </td> 

            <td>       
            <!-- <img src="{{url('public/Image/'.$data->logo)}}" -->
            <img src="{{ asset($data->logo) }}"
            style="height: 100px; width: 100px;">
	          </td> 

            <td>

              <a href="{{route('shop.edit',$data->id)}}" class="btn btn-primary">Edit</a>        
              <form action="{{route('shop.destroy',$data->id)}}" method="POST">
              <button class="btn btn-danger">
                  @csrf
                  @method('DELETE')
                Delete
              </button>
              </form>
              
            </td>
        
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>