@extends('welcome')

@section('content')

<div class="container">
    <h3>View all items</h3><hr>

    <a class="btn btn-success" href="{{route('items.add')}}" style="text-decoration:none;padding:10px;">
            Create Items +
          </a>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Item ID</th>
          <th scope="col">Name</th>
          <th scope="col">Cover</th>
          <th scope="col">Image</th>
          <th scope="col">Price</th>
          <th scope="col">Menu</th>
          <th scope="col">Shop</th>
          <th scope="col">Action</th>

        </tr>
      </thead>
      <tbody>
        @foreach($imageData as $data)
        <tr>
            <td>{{$data->id}}</td>

            <td>
                {{$data->name}}
            </td>  

            <td>
            <img src="{{asset($data->cover)}}"
            style="height: 100px; width:100px;">
	        </td> 

            <td>

            @foreach(explode(', ', $data->image) as $path)
            <img src="{{asset($path)}}" style="height: 100px; width: 150px;">
            @endforeach
           
	        </td> 

            <td>
            {{$data->price}}
            </td> 

            <td>
                {{$data->menu->name ?? 'unknown'}}
            </td> 

            <td>
                {{$data->shop->name ?? 'unknown'}}
            </td>

            <td>

                <a href="{{route('item.edit',$data->id)}}" class="btn btn-primary">Edit</a>
                
                <form action="{{route('item.destroy',$data->id)}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger">
                      DELETE
                  </button>
                </form>
            </td>
            
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>