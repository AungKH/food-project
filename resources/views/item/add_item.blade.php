@extends('welcome')

@section('content')


<div class="container">
  <form method="post" action="{{ route('items.store') }}" 
		enctype="multipart/form-data">
    @csrf

    <div class="name">
        <label><h4>Add Name</h4></label>
        <input type="text" class="form-control" required name="name">
    </div><br>

    <div class="cover">
      <label><h4>Add Cover</h4></label>
      <input type="file" class="form-control" required name="cover">
    </div><br>

    <div class="image">
      <label><h4>Add Image</h4></label>
      <input type="file" class="image" required name="images[]" multiple>
    </div><br>

    <div class="price">
        <label><h4>Add Price</h4></label>
        <input type="text" class="form-control" required name="price">
    </div><br>

    <div class="shop_c">
      <label><h4>Shop</h4></label>
      <select name="shop_id" class="form-control">
        <option value=" " disabled selected>--Select Shop--</option>
      @foreach($shops as $shop)
        <option value="{{$shop->id}}" >{{$shop->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>

    <div class="menu_c">
      <label><h4>Menu</h4></label>
      <select name="menu_id" class="form-control">
        <option value=" " disabled selected>--Select Menu--</option>
      @foreach($menus as $menu)
        <option value="{{$menu->id}}" >{{$menu->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>

    



    <div class="post_button">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </form>
</div>