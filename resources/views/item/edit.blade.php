@extends('welcome')

@section('content')


<div class="container">
  <form method="post" action="{{route('item.update',$item->id)}}"  
		enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="name">
        <label><h4>Add Name</h4></label>
        <input type="text" class="form-control" value="{{$item->name}}" name="name">
    </div><br>

    <div class="cover">
      <label><h4>Old Cover</h4></label>
      <input type="hidden"  name="old_cover" value="{{$item->cover}}">
      <img src="{{asset($item->cover)}}" width="100px">
    </div><br>

    <div class="cover">
      <label><h4>New Cover</h4></label>
      <input type="file" class="form-control"  name="new_cover">
    </div><br>

    <div class="image">
      <label><h4>Old Image</h4></label>
      <input type="hidden" class="image"  name="oldimages[]" value="{{$item->image}}" multiple>
      @foreach(explode(', ',$item->image) as $path)
      <img src="{{asset($path)}}" width="100px">
     @endforeach
    </div><br>

    <div class="image">
      <label><h4>New Image</h4></label>
      <input type="file" class="image"  name="newimages[]"  multiple>
      
    </div><br>

    <div class="price">
        <label><h4>Add Price</h4></label>
        <input type="text" class="form-control" value="{{$item->price}}" name="price">
    </div><br>

    <div class="shop_c">
      <label><h4>Shop</h4></label>
      <select name="shop_id" class="form-control">
        <option value=" " disabled selected>--Select Shop--</option>
      @foreach($shops as $shop)
        <option  @if($shop->id==$item->shop_id) selected="selected"
                @endif
        value="{{$shop->id}}" >{{$shop->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>

    <div class="menu_c">
      <label><h4>Menu</h4></label>
      <select name="menu_id" class="form-control">
        <option value=" " disabled selected>--Select Menu--</option>
      @foreach($menus as $menu)
        <option @if($menu->id==$item->menu_id) selected="selected"
                @endif
        value="{{$menu->id}}" >{{$menu->name ?? ''}}</option>
        @endforeach
      </select> 
    </div><br>

    <div class="post_button">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </form>
</div>