<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\ItemController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});;

Route::get('/add-menu',[MenuController::class,'addMenu'])->name('menus.add');

//For storing an image
Route::post('/store-menu',[MenuController::class,'storeMenu'])->name('menus.store');

//For showing an image
Route::get('/view-menu',[MenuController::class,'viewMenu'])->name('menus.view');

Route::get('/update-menu',[MenuController::class,'update'])->name('menus.update');
Route::resource('menu',MenuController::class);



//Shop//////

Route::get('/add-shop',[ShopController::class,'addShop'])->name('shops.add');

Route::post('/store-shop',[ShopController::class,'storeShop'])->name('shops.store');

Route::get('/view-shop',[ShopController::class,'viewShop'])
->name('shops.view');

Route::resource('shop',ShopController::class);


//Route for item
Route::get('/add-item',[ItemController::class,'addItem'])->name('items.add');

Route::post('/store-item',[ItemController::class,'storeItem'])->name('items.store');

Route::get('/view-item',[ItemController::class,'viewItem'])
->name('items.view');

Route::resource('item',ItemController::class);

