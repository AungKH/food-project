<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Shop;

class ShopController extends Controller
{

    public function addShop(){
        return view('shop.add_shop');
    }
    
    public function storeShop(Request $request){
        $data= new Shop();

         $data['name'] =  $request->name ;
         $data['address'] =  $request->address ;
        if($request->file('logo')){
            $file= $request->file('logo');
            $path="storage/images/logo/";
            $filename= date('YmdHi').$file->getClientOriginalName();
            // $file-> move(public_path('public/Image'), $filename);
            $file->storeAs('public/images/logo/',$filename);
            $data['logo']= $path.$filename;
        }

        $data->save();
        return redirect()->route('shops.view');
    }
		//View image
    public function viewShop(){
        $imageData= Shop::all();
        return view('shop.view_shop',compact('imageData'));
    }

    public function edit(Shop $shop){
        return view('shop.edit',['shop'=>$shop]);
    }

    public function update(Request $request,Shop $shop){
        $inputs=$request->all();
        $image=null;
        if(isset($inputs['new_logo'])){
            if($request->file('new_logo')){
                $file= $request->file('new_logo');
                $path="storage/images/logo/";
                $filename= date('YmdHi').$file->getClientOriginalName();
                // $file-> move(public_path('public/Image'), $filename);
                $file->storeAs('public/images/logo/',$filename);
                
                $image= $path.$filename;
                
                if(file_exists($shop->image)) {
                    
                    unlink($shop->image);
                }
            }

        }else{
            $image = $inputs['old_logo'];
        }
        
        $shop->name=$inputs['name'];
        $shop->address=$inputs['address'];
        $shop->logo=$image;
        $shop->save();
        return redirect()->route('shops.view')->with('success','successfully Updated');
    }

    
    public function destroy(Shop $shop){
            $shop->delete();
            return redirect()->route('shops.view')->with('success','Sucessfully Deleted');
    }
    
    
}
