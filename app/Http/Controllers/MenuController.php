<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    public function addMenu(){
        return view('menu.add_menu');
    }
    
    public function storeMenu(Request $request){
        $data= new Menu();
        if($request->file('image')){
            $file= $request->file('image');
            $path="storage/images/menu/";
            $filename=date('YmdHi').$file->getClientOriginalName();
            $file->storeAs('public/images/menu/',$filename);
        
           // $file-> move(public_path('public/Image'), $filename);
            $data['image']= $path.$filename;
        }

        $data['name'] =  $request->name ;
        $data->save();

       
        return redirect()->route('menus.view');
    }
		//View image
    public function viewMenu(){
        $imageData= Menu::all();
        return view('menu.view_menu',compact('imageData'));
    }

    public function edit(Menu $menu){
        return view('menu.edit',['menu'=>$menu]);
    }

    public function update(Request $request,Menu $menu){
        // dd($request);
        $inputs = $request->all(); 
        $image = null;
        if(isset($inputs['new_image'])) {
            if($request->file('new_image')){
                $file= $request->file('new_image');
                $path="storage/images/menu/";
                $filename=date('YmdHi').$file->getClientOriginalName();
                $file->storeAs('public/images/menu/',$filename);
            
               // $file-> move(public_path('public/Image'), $filename);

                $image= $path.$filename;
                
                if(file_exists($menu->image)) {
                    
                    unlink($menu->image);
                }
                
            }
    
        }else{
            $image = $inputs['old_image'];
        }

        $menu->name = $inputs['name'];
        $menu->image = $image;
        $menu->save();
        return redirect()->route('menus.view')->with('success','successfully Updated');
    }

    public function destroy(Menu $menu){
        $menu->delete();
        return redirect()->route('menus.view')->with('success','Successfully deleted');
    }

    
}
