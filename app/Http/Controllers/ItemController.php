<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Menu;
use App\Models\Shop;

class ItemController extends Controller
{
    public function addItem(){
        $menus = Menu::all();
        $shops = Shop::all();
        return view('item.add_item')->with([
            'menus' => $menus,
            'shops' => $shops
        ]);
    }

    public function storeItem(Request $request){
            $data = new Item();
            $data['name'] = $request->name;

            if($request->file('cover')){
                $file= $request->file('cover');
                $path="storage/images/item/";
                $filename=date('YmdHi').$file->getClientOriginalName();
                $file->storeAs('public/images/item/',$filename);
            
               // $file-> move(public_path('public/Image'), $filename);
                $data['cover']= $path.$filename;
            }

            if($request->file('images')){
                $files= $request->file('images');

                foreach($files as $file){

                    $filename=date('YmdHi').$file->getClientOriginalName();              
                    $path="storage/images/item/";
                    $file->storeAs('public/images/item/',$filename);
                    $imgs[]=$path.$filename;
                }

                $imgstr = implode(", ",$imgs);
                $data['image']=$imgstr;  
            
               // $file-> move(public_path('public/Image'), $filename);
                
            }

            $data['price']=$request->price;
            $data['shop_id']=$request->shop_id;
            $data['menu_id']=$request->menu_id;
            $data->save();

            return redirect()->route('items.view');


    }

    public function viewItem(){
        $imageData= Item::all();
        // dd($imageData);
        return view('item.view_item',compact('imageData'));
    }

    public function edit(item $item){
        $menus = Menu::all();
        $shops = Shop::all();
        return view('item.edit',['item'=>$item])->with([
            'menus' => $menus,
            'shops' => $shops
        ]); 
        // return view('item.edit',['item'=>$item]);
    }

    public function update(Request $request,Item $item){
        
        $inputs = $request->all();
        $image = null;
        $cover= null;
        if(isset($inputs['new_cover'])) {
            if($request->file('new_cover')){
                $file= $request->file('new_cover');
                $path="storage/images/item/";
                $filename=date('YmdHi').$file->getClientOriginalName();
                $file->storeAs('public/images/item/',$filename);
                $cover = $path.$filename;

                if(file_exists($item->image)){
                    unlink($item->image);
                }
                 
            }
        }else{
            $cover = $inputs['old_cover'];
        }

        if(isset($inputs['newimages'])) {
            if($request->file('newimages')){
                $files= $request->file('newimages');
                foreach($files as $file){
                    $filename=date('YmdHi').$file->getClientOriginalName();
                    $path="storage/images/item/";
                    $file->storeAs('public/images/item/',$filename);
                    $img[]= $path.$filename;
                }
                    
                $image=implode(", ",$img);
                
                if(file_exists($item->image)){
                    unlink($item->image);
                }
            }
        }else{
            $image = $inputs['oldimages'];
        }



        $item->name=$inputs['name'];
        $item->price=$inputs['price'];
        $item->cover=$cover;
        $item->image=$image;
        $item->shop_id=$inputs['shop_id'];
        $item->menu_id=$inputs['menu_id'];
        $item->save();
        return redirect()->route('items.view')->with('success','successfully Updated');
    }
    
    public function destroy(Item $item){
            $item->delete();
            return redirect()->route('items.view')->with('success','Successfully deleted');
    }
}
