<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $fillable =['name','cover','image','price','menu_id','shop_id'];


    public function menu() {
        return $this->belongsTo(Menu::class);
    }

    public function shop(){
        return $this->belongsTo(Shop::class);
    }
}
